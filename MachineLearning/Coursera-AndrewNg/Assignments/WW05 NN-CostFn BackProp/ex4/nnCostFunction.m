function [J grad] = nnCostFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification
%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices. 
% 
%   The returned parameter grad should be a "unrolled" vector of the
%   partial derivatives of the neural network.
%

% Reshape nn_params back into the parameters Theta1 and Theta2, the weight matrices
% for our 2 layer neural network
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));

% Setup some useful variables
m = size(X, 1);
         
% You need to return the following variables correctly 
J = 0;
Theta1_grad = zeros(size(Theta1));
Theta2_grad = zeros(size(Theta2));

% ====================== YOUR CODE HERE ======================
% Instructions: You should complete the code by working through the
%               following parts.
%
% Part 1: Feedforward the neural network and return the cost in the
%         variable J. After implementing Part 1, you can verify that your
%         cost function computation is correct by verifying the cost
%         computed in ex4.m
%
% Part 2: Implement the backpropagation algorithm to compute the gradients
%         Theta1_grad and Theta2_grad. You should return the partial derivatives of
%         the cost function with respect to Theta1 and Theta2 in Theta1_grad and
%         Theta2_grad, respectively. After implementing Part 2, you can check
%         that your implementation is correct by running checkNNGradients
%
%         Note: The vector y passed into the function is a vector of labels
%               containing values from 1..K. You need to map this vector into a 
%               binary vector of 1's and 0's to be used with the neural network
%               cost function.
%
%         Hint: We recommend implementing backpropagation using a for-loop
%               over the training examples if you are implementing it for the 
%               first time.
%
% Part 3: Implement regularization with the cost function and gradients.
%
%         Hint: You can implement this around the code for
%               backpropagation. That is, you can compute the gradients for
%               the regularization separately and then add them to Theta1_grad
%               and Theta2_grad from Part 2.
%

% -------------------------------------------------------------







%{
%%Part 1: ---- My code -- Begin --- didn't work
% My Notes: X = 5000x400 matrix. y = 5000x1(numeric) matrix
% Theta1 = 25x401 matrix. Theta2 = 10x26 matrix
% m = 5000; num_labels = 10; 

% Add ones to the X data matrix
X = [ones(m, 1) X];  %5000x401 matrix

% Converting y (digital labels) to logical vector. yt is 5000x10 matrix
yt = (y == 1);
for c = 2:num_labels
  temp =(y == c);
  yt = [yt temp];
end;

sum = 0;


for i = 1:m

	% Compute hidden layer (2) Z2 values
	Z2 =  Theta1 * X(i, :)';  %X(i, :) = 1x401 vector; Theta1 is 25x401 matrix; Z2 is 25x1 vector
	a2 = sigmoid(Z2);       % a2 = g(Z2); 25x1 vector
	a2_size = size(a2);
	a2 = [1;  a2 ]; %add a0 = 1 (bias); 26x1 vector

	% Compute output layer (3) Z3 values
	Z3 = Theta2 * a2;  %Theta2 = 10x26 matrix; a2 is 26x1 vector; Z3 is 10X1 matrix
	a3 = sigmoid(Z3);  %a3 is 10x1 vector
	h = a3;
	
    for k = 1:num_labels
		sum = sum+ (-yt(i, k) * log(a3(k, 1)) - (1-yt(i, k) * log(1-a3(k, 1))  ));
    end;

end;

J = sum;

%%Part 1: ---- My code -- End ---

%}
% -------------------------------------------------------------












%%Part 1: ---- Another Approach -- Begin ---
% My Notes: X = 5000x400 matrix. y = 5000x1(numeric) matrix
% Theta1 = 25x401 matrix. Theta2 = 10x26 matrix
% m = 5000; num_labels = 10; 


% Add ones to the X data matrix
X = [ones(m, 1) X];  %5000x401 matrix

% Converting y (digital labels) to logical vector. yt is 5000x10 matrix
y_matrix = eye(num_labels)(y,:);


a1 = X; %5000x401 matrix

z2 = a1 * Theta1'; %Theta1 is 25x401 matrix; Z2 is 5000x25 matrix
a2 = sigmoid(z2); %a2 is 5000x25 vector

a2_ROWSIZE = size(a2, 1);
a2 = [ones(a2_ROWSIZE,1)  a2 ]; %add a0 = 1 (bias); 5000x26 matrix

z3 = a2 * Theta2'; %a2 is 5000x26 matrix; Theta2 is 10x26 matrix; 
a3 = sigmoid(z3); %a3 and z3 are 5000x10 matrix
h = a3;
%size(a3)


% Compute the sum of the diagonal elements using the "trace()" command, or by sum(sum(...)) after element-wise multiplying by an identity matrix of size (K x K).
% The sum-of-product terms that are NOT on the main diagonal are unwanted - they are not part of the cost calculation.
% So simply using sum(sum(...)) over the matrix product will include these terms, and you will get an incorrect cost value.
J = ((1/m) * sum(sum((-y_matrix .* log(h))-((1-y_matrix) .* log(1-h)))));


% Regularized cost function
reg = (lambda/(2*m)) * (sum(sum((Theta1(:,2:end)).^2)) + sum(sum((Theta2(:,2:end)).^2)));
J = J + reg;

% -------------------------------------------------------------

%Part 2: Implementing the backpropagation algorithm


%Step1 - Performing a feedforward pass, computing the activations (z(2),a(2),z(3),a(3))  for layers 2 and 3
% Already computed ~ z2, a2, z3, a3 in Part 1


%Step2 - For each output unit k in layer 3 (the output layer), set delta3 = a3-y 
delta3 = a3-y_matrix;

%Step3 - For the hidden layer l = 2, set δ(2) =Θ(2)T δ(3).*g'(z(2)) 
% Theta2 is 10x26 matrix. delta 3 is 5000x10 matrix.
% z2_bias = [ones(size(z2, 1), 1 )  z2 ] %Z2_bias is 5000x26 matrix
delta2 = (delta3 * Theta2(:, 2:end)) .* sigmoidGradient(z2);   %delta3 (5000x10) * Theta2(:, 2:end) -10x25 = 5000x25 .* sigmoid(Z2) 5000x10


%Step4 -  Accumulate the gradient from this example using the following formula. Note that you should skip or remove δ(2)0 
Big_delta2 = delta3' * a2; 
Big_delta1 = delta2' * a1;


%Step5 - Obtain the (unregularized) gradient for the neural network cost function by dividing the accumulated gradients by 1/m: 
Theta1_grad = 1/m * (Big_delta1);
Theta2_grad = 1/m * (Big_delta2);


% -------------------------------------------------------------

%Part 3: Implementing regularization

%Note that you should not be regularizing the ﬁrst column of Θ(l) which is used for the bias term
% 6. set the first column of Theta1 and Theta2 to all-zeros
Theta1(:, 1) = 0;
Theta2(:, 1) = 0;


% 7. Scale each Theta matrix by λ/m
Theta1 = lambda/m * Theta1;
Theta2 = lambda/m * Theta2;


% 8. Add each of these modified-and-scaled Theta matrices to the un-regularized Theta gradients that you computed earlier.
Theta1_grad = Theta1_grad + Theta1;
Theta2_grad = Theta2_grad + Theta2;


% =========================================================================

% Unroll gradients
grad = [Theta1_grad(:) ; Theta2_grad(:)];


end
