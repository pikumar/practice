function [theta, J_history] = gradientDescentMulti(X, y, theta, alpha, num_iters)
%GRADIENTDESCENTMULTI Performs gradient descent to learn theta
%   theta = GRADIENTDESCENTMULTI(x, y, theta, alpha, num_iters) updates theta by
%   taking num_iters gradient steps with learning rate alpha

% Initialize some useful values
m = length(y); % number of training examples
J_history = zeros(num_iters, 1);

for iter = 1:num_iters

    % ====================== YOUR CODE HERE ======================
    % Instructions: Perform a single gradient step on the parameter vector
    %               theta. 
    %
    % Hint: While debugging, it can be useful to print out the values
    %       of the cost function (computeCostMulti) and gradient here.
    %

%{}
h = X * theta;
h = (h - y);

sum = 0;
for i = 1:m
 sum = sum + h(i, 1);
end;
J = 1/(2*m) * sum;
%}

    NUM_FEATURES = size(X, 2);
	sumv = zeros(NUM_FEATURES, 1);
	diffv = zeros(NUM_FEATURES, 1);
	h = 0;
	
for k = 1:NUM_FEATURES

	for i = 1:m
	
	  for j = 1:NUM_FEATURES
	    h = h + theta(j, 1) * X(i, j);
	  end; 
	  
	  diffv(k, 1) = diffv(k, 1) + (h - y(i, 1)) * X(i, k);
	  
	  h = 0;
	end;
	
	%theta(k, 1) = theta(k, 1) - alpha*(1/m)*diffv(k, 1);
end;

for k = 1:NUM_FEATURES

	theta(k, 1) = theta(k, 1) - alpha*(1/m)*diffv(k, 1);
end;

    % ============================================================

    % Save the cost J in every iteration    
    J_history(iter) = computeCostMulti(X, y, theta);

end

end
