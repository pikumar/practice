function J = computeCost(X, y, theta)
%COMPUTECOST Compute cost for linear regression
%   J = COMPUTECOST(X, y, theta) computes the cost of using theta as the
%   parameter for linear regression to fit the data points in X and y

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta
%               You should set J to the cost.

%% h(X) = (theta)'*X -- won't work
%% h(x) = theta(0)*x(0) + theta(1)*x(1)
%% J(theta) = 1/(2*m) SUM[ h(x) - y ]^2
sum = 0;
diff = 0;
h = 0;

for i = 1:m
  h = theta(1, 1) * X(i, 1) + theta(2, 1) * X(i, 2);
  diff = [h - y(i, 1)]^2;
  sum = sum + diff;
end;

J = 1/(2*m) * sum;



% =========================================================================

end
