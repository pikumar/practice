function [theta, J_history] = gradientDescent(X, y, theta, alpha, num_iters)
%GRADIENTDESCENT Performs gradient descent to learn theta
%   theta = GRADIENTDESCENT(X, y, theta, alpha, num_iters) updates theta by 
%   taking num_iters gradient steps with learning rate alpha

% Initialize some useful values
m = length(y); % number of training examples
J_history = zeros(num_iters, 1);

for iter = 1:num_iters

    % ====================== YOUR CODE HERE ======================
    % Instructions: Perform a single gradient step on the parameter vector
    %               theta. 
    %
    % Hint: While debugging, it can be useful to print out the values
    %       of the cost function (computeCost) and gradient here.
    %

    temp0 = 0; temp1 = 0;

	sum0 = 0; sum1 = 0;
	diff0 = 0; diff1 = 0;
	h = 0;

	for i = 1:m
	  h = theta(1, 1) * X(i, 1) + theta(2, 1) * X(i, 2);
	  
	  diff0 = [h - y(i, 1)];
	  diff1 = diff0 * X(i, 2);
	  
	  sum0 = sum0 + diff0;
  	  sum1 = sum1 + diff1;

	end;
	
	theta(1, 1) = theta(1, 1) - alpha*(1/m)*sum0;
	theta(2, 1) = theta(2, 1) - alpha*(1/m)*sum1;

	%%pi- fprintf('Iter- %.0f -- theta: ', iter); fprintf(mat2str(theta));fprintf(' -- J = ');

	%{
	J_theta0 = 1/(2*m) * sum0;
    J_theta1 = 1/(2*m) * sum1;

	temp0 = theta(1, 1) - alpha * J_theta0;
    temp1 = theta(2, 1) - alpha * J_theta1;

    theta(1, 1) = temp0;
    theta(2, 1) = temp1;
	%}

    % ============================================================

    % Save the cost J in every iteration    
    J_history(iter) = computeCost(X, y, theta);
	%%pi- fprintf('%.6f \n', J_history(iter));  

end

end
