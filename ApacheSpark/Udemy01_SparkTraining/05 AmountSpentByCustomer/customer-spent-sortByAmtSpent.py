from pyspark import SparkConf, SparkContext

conf = SparkConf().setMaster("local").setAppName("Customer Spent Amount")
sc = SparkContext(conf = conf)

def parseLine(line):
    fields = line.split(',')
    custId = int(fields[0])
    amtSpent = float(fields[2])
    return (custId, amtSpent)

lines = sc.textFile("file://///home/appladm/Practice/U05_AmountSpentByCustomer/customer-orders.csv")
rdd = lines.map(parseLine)

totalsByCustId = rdd.reduceByKey(lambda x, y: (x + y))

swapRdd = totalsByCustId.map(lambda (x,y):(y,x)).sortByKey(0)   #0 - desc, 1-asc

results = swapRdd.collect()
for result in results:
    print(result)
