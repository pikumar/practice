from pyspark import SparkConf, SparkContext
import collections

conf = SparkConf().setMaster("local").setAppName("Popular Movie")
sc = SparkContext(conf = conf)


def parseLine(line):
    fields = line.split()
    movieId = int(fields[1])
    ratings = int(fields[2])
    return (movieId)
	
	
lines = sc.textFile("file:////home/appladm/Practice/U01_MovieRatings/ml-100k/u.data")
rdd = lines.map(parseLine)

movieRdd = rdd.map(lambda x:(x,1)).reduceByKey(lambda x, y: (x + y)).map(lambda (x,y):(y,x)).sortByKey(0)

results = movieRdd.collect()
for result in results:
    print(result)
